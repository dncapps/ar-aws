AR-aws
=========

Generic placeholder for cloud tasks. Currently holds proof of concept tasks. Discretion is advised when adding to this repository to avoid creating another monolith.

Requirements
------------

Boto Version : {{ insert value here }}

Role Variables
--------------

Sandbox Flag: {{ TBD }}
Production Flag: {{ TBD }}
VPC Flag: {{ TBD }}

Dependencies
------------

Example Playbook
----------------

License
-------

BSD

Contact Information
------------------

Cloud solutions experts at dnc.